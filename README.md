# Docker.
## Requirements.

Linux. 

Docker 20.10.2+. 

Docker compose 1.25.0+.

## Install soft.
```
    sudo apt update && apt upgrade
    sudo apt install docker docker.io containerd docker-compose git
```

## Get docker.
```
    mkdir ibotgroup && cd ibotgroup
    git clone git@gitlab.com:ibotgroup/docker.git
```

##	Development environment.
```
    git clone git@gitlab.com:ibotgroup/backend.git
    git clone git@gitlab.com:ibotgroup/frontend.git
    
    sudo sh -c "echo '127.0.0.1 insiderbot.local' >> /etc/hosts"
    sudo sh -c "echo '127.0.0.1 api.insiderbot.local' >> /etc/hosts"
    
    cd ibotgroup
    mkdir -p secrets/dev/mysql
    echo 'password' > secrets/dev/mysql/mysql_root_password
    
    cd docker
    sudo docker-compose -f docker-compose.dev.yml up --build
    
    sudo docker container exec -it docker_bot_dev_mysql_1 mysql -uroot -p
    CREATE USER 'insiderbot'@'%' IDENTIFIED BY 'password';
    GRANT ALL ON insiderbot.* TO 'insiderbot'@'%';
    exit;
    
    sudo docker exec -i docker_bot_dev_mysql_1 sh -c 'exec mysql -uinsiderbot -p"password" insiderbot' < dump.sql
```

##	Development compose.
```
    cd docker
    sudo docker-compose -f docker-compose.dev.yml up --build
```

##	Production environment: before the first launch.
```
    cd ibotgroup
    
    cd docker
    bash fetch.bash {backend_tag} {frontend_tag}
    sudo docker-compose -f build.yml build
    cd ..
    
    mkdir -p secrets/main/redis/
    echo "password" > secrets/main/redis/password #set your password
    
    mkdir -p secrets/main/mysql
    echo 'password' > secrets/main/mysql/mysql_root_password
    
    mkdir -p secrets/main/ssl
    cp /ssl/api/fullchain.pem secrets/main/ssl/api/fullchain.pem
    cp /ssl/api/privkey.pem secrets/main/ssl/api/privkey.pem
    cp /ssl/bot/fullchain.pem secrets/main/ssl/bot/fullchain.pem
    cp /ssl/bot/privkey.pem secrets/main/ssl/bot/privkey.pem
    
    mkdir -p secrets/main/app/
    cp docker/git/backend/.env.example secrets/main/app/.env #than change all passwords and other accesses.
    
    mkdir -p secrets/main/supervisor
    cp docker/main/supervisor/supervisord.conf secrets/main/supervisor/supervisord.conf #than change http auth password.
    
    sudo docker-compose -f stack.yml up
    
    sudo docker container exec -it docker_bot_mysql_1 mysql -uroot -p
    CREATE USER 'insiderbot'@'%' IDENTIFIED BY 'password';
    GRANT ALL ON insiderbot.* TO 'insiderbot'@'%';
    exit;
    
    sudo docker exec -i docker_bot_mysql_1 sh -c 'exec mysql -uinsiderbot -p"password" insiderbot' < dump.sql
    
    sudo chown -R 82:82 ../data_main/logs/backend/
    sudo chmod -R 755 ../data_main/logs/backend/
    
    #then press CTRL+C.
```

##	Production deploy.
```
    cd docker
    bash fetch.bash {backend_tag} {frontend_tag}
    sudo docker-compose -f build.yml build
    sudo docker stack deploy -c stack.yml bot
    sudo docker service update --publish-add published=4273,target=443,mode=host bot_bot_nginx
```

##	Production update: before redeploy.
```
    sudo docker container exec -it docker_bot_horizon_1 php artisan horizon:terminate --wait
```

##	Production update: redeploy.
```
    sudo docker stack rm bot
    #see Production deploy.
```

##	Production compose (test mode).
```
    cd docker
    sudo docker-compose -f stack.yml up
```

## Other.
```
    sudo docker service update --publish-rm published=8273,target=80 --publish-rm published=4273,target=443 bot_bot_nginx #remove port
```

## Configure db backups.

insert to crontab "*/30 * * * * cd /var/www/ibotgroup/docker/scripts/ && bash backup.bash"

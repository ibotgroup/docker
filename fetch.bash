#bash

backend_tag=
frontend_tag=

fetch () {
  echo "Cloning && Fetching"
  rm -rf git/
  git clone git@gitlab.com:ibotgroup/backend.git git/backend
  cd git/backend

  if git tag | grep -q $backend_tag;
    then
      echo "Switching backend to tag $backend_tag"
    else
      echo "Tag not found. Break"
      exit
  fi

  git checkout $backend_tag
  cd ../..

  git clone git@gitlab.com:ibotgroup/frontend.git git/frontend
  cd git/frontend

  if git tag | grep -q $frontend_tag;
    then
      echo "Switching frontend to tag $frontend_tag"
    else
      echo "Tag not found. Break"
      exit
  fi

  git checkout $frontend_tag
}

if [ "$#" !=  "2" ]
   then
     echo "No arguments supplied"
   else
     backend_tag=$1
     frontend_tag=$2
     fetch
 fi

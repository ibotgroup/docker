FROM bot_fpm_base

RUN apk add --update python2 alpine-sdk npm

RUN mkdir /frontend
WORKDIR /frontend
COPY git/frontend/ .
ADD main/base/frontend/.env .env
RUN npm install && npm run build

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');" && \
    mv composer.phar /usr/local/bin/composer

RUN mkdir /app
WORKDIR /app
COPY git/backend .
RUN rm -rf .git/ server/
RUN composer update --prefer-dist --no-dev --optimize-autoloader --classmap-authoritative
RUN php artisan vendor:publish --force --tag=horizon-assets


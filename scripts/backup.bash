#bash
mkdir -p ../../backups
exec &>> ../../backups_log

find ../../backups/ -type f ! -newermt "$(date --date="1 day ago")" -exec rm {} \;

mkdir -p ../../backups
docker container exec $(docker ps --format "{{.Names}}" | grep bot_bot_mysql.1)  sh -c 'MYSQL_PWD="'$(cat ../../secrets/main/mysql/mysql_root_password)'" exec mysqldump -uroot insiderbot' | gzip > ../../backups/$(date +%Y-%m-%d_%H:%M:%S).sql.gz

